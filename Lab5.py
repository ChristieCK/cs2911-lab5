# coding=utf-8
# httpclient.py Lab 3 -- HTTP client
# Team members: Jamie Doro (doroja), Connor Christie (username2)

# import the "socket" module -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# import the "regular expressions" module
import re


def main():
    # this resource request should result in "chunked" data transfer
    get_http_resource('http://seprof.sebern.com/', 'index-file.md')
    # this resource request should result in "Content-Length" data transfer
    get_http_resource('http://seprof.sebern.com/sebern1.jpg', 'sebern1.jpg')
    get_http_resource('http://seprof.sebern.com:8080/sebern1.jpg', 'sebern2.jpg')


# another resource to try for a little larger and more complex entity
#    get_http_resource('http://seprof.sebern.com/courses/cs2910-2014-2015/sched.md','sched-file.md')

# Get an HTTP resource from a server
#        Parse the URL and call function to actually make the request.
#        url -- full URL of the resource to get
#        file_name -- name of file in which to store the retrieved resource
# (do not modify this function)
def get_http_resource(url, file_name):
    # Parse the URL into its component parts using a regular expression.
    url_match = re.search('http://([^/:]*)(:\d*)?(/.*)', url)
    url_match_groups = url_match.groups() if url_match else []
    #    print 'url_match_groups=',url_match_groups
    if len(url_match_groups) == 3:
        host_name = url_match_groups[0]
        host_port = int(url_match_groups[1][1:]) if url_match_groups[1] else 80
        host_resource = url_match_groups[2]
        print('host name = {0}, port = {1}, resource = {2}'.format(host_name, host_port, host_resource))
        status_string = make_http_request(host_name.encode(), host_port, host_resource.encode(), file_name)
        print('get_http_resource: URL="{0}", status="{1}"'.format(url, status_string))
    else:
        print('get_http_resource: URL parse failed, request not sent')


# Get an HTTP resource from a server
#     host -- bytes object with the ASCII domain name or IP address of the server machine (i.e., host) to connect to
#     port -- integer port number to connect to on host
#     resource -- bytes object with the ASCII path/name of resource to get. This is everything in the URL after the domain name, including the
#                 first /.
#     file_name -- string (str) containing name of file in which to store the retrieved resource
#
# Returns the status code as an integer
# Writen by: Jamie Doro and Connor Christie
def make_http_request(host, port, resource, file_name):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.send(b"GET %s HTTP/1.1\r\nHost: %s\r\n\r\n" %(resource, host))

    response_headers = read_response_header(s)
    headers = read_headers(s)

    if 'Transfer-Encoding' in headers and headers["Transfer-Encoding"] == "chunked":
        message = read_chunked_response(s)
    else:
        message = read_content_legnth_response(s, headers["Content-Length"])

    output_to_file(message, file_name)

    return response_headers[1]  # Replace this "server error" with the actual status code


# Reads bytes until a CR LF is read
# Returns the content of the whole line
# Written By: Jamie Doro
def read_line(connection):
    message = ""
    while not message.endswith("\r\n"):
        message += next_character(connection)

    return message.replace("\r\n", "")


# Returns the next character for each byte
# Written By: Jamie Doro and Connor Christie
def next_character(connection):
    return chr(int.from_bytes(next_byte(connection), 'big'))


# reads the body of the http response if it is chunked
# Written By: Connor Christie
def read_chunked_response(connection):
    body = b""
    chunk_length = int(read_line(connection), 16)

    while chunk_length >0:
        body += read_chunked_body(connection, chunk_length)

        chunk_length = int(read_line(connection), 16)

    return body


# reads the a chunk of the body
# Written by: Connor Christie
def read_chunked_body(connection, chunk_length):
    body = b""

    for x in range(0, chunk_length):
        body += next_byte(connection)

    next_byte(connection)
    next_byte(connection)

    return body


# reads the body of the http response if a content-length is given
# Written by: Connor Christie
def read_content_legnth_response(connection, content_length):
    message = b""
    for x in range (0, int(content_length)):
        message += next_byte(connection)

    return message


# Parses the response header and returns a tuple consisting of the version, status and description
# Written By: Jamie Doro
def read_response_header(connection):
    message = read_line(connection)
    parts =  message.split(" ")
    version = parts[0]
    status = parts[1]
    description = parts[2]
    return (version, status, description)


# Reads the headers of the HTTP response
# Returns a dictionary of the headers
# Written by Jamie Doro
def read_headers(connection):
    content__dictionary = {}
    message = read_line(connection)

    while message != "":
        header_content = message
        parts = header_content.split(": ")
        content__dictionary[parts[0]] = parts[1]
        message = read_line(connection)
    return content__dictionary


# Opens a file to be written to and then closes it
# Written by Jamie Doro and Connor Christie
def output_to_file(message, filename):
    output_file = open(filename, "wb")
    output_file.write(message)
    output_file.close()


# Read the next byte from the socket data_socket.
# The data_socket argument should be an open tcp data connection socket, not a tcp listening socket.
# Returns the next byte, as a bytes object of one character.
# If the byte is not yet available, this method blocks (waits)
#   until the byte becomes available.
# If there are no more bytes, this method blocks indefinitely.
def next_byte(connection):
    return connection.recv(1)


main()